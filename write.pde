import processing.serial.*;

Serial serial;
String value;
PrintWriter output;
int count = 1;
int len = 2000;

void setup(){
  serial = new Serial(this, Serial.list()[0], 115200);
  serial.bufferUntil('\n');
  output = createWriter("test.csv");
}

void draw(){
}

void serialEvent(Serial serial){
  if(count <= len){
    value = serial.readString();
    
    if(value != null){
     output.print(trim(value) + (count == len ? "" : ",")); 
     count++;
    }
  }else{
    output.flush();
    output.close();
    exit();
  }
}
